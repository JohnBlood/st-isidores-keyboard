+++
date = "2016-04-11T22:11:03-04:00"
draft = false
title = "About"

+++

## Author

![](/img/headshot.png )

This website is created by John Paul Wohlscheid. John Paul is a big fan of technology. He remembers using Windows 3.1.1 when he was younger (pre-teens), which tells you how old he is. He wishes that he could write programs and become fabulously wealthy, but he can barely write basic HTML. In the meantime, he likes to read and write about technology. John Paul has published a number of ebooks. You can check out his books on [his website](http://johnpaulwohlscheid.work/). He mainly writes detective fiction, for now.

## Who is St. Isidore?

![](/img/patron-computers.jpg )

St. Isidore of Seville is the patron saint of computers, computer users, computer programmers, and the Internet. I ask for his patronage as I write this blog.

## How was this site built?

This site was built using [Hugo](https://gohugo.io/). This site is hosted on [GitLab](http://gitlab.com/). You can see the site's repo [here](https://gitlab.com/JohnBlood/st-isidores-keyboard) if you are so inclined.

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

If you find any of the information on this site interesting, please take a sec send me a buck or two. The content is free, but the other stuff does. You can use [Paypal.me](https://paypal.me/johnpaulw) or [Square Cash](https://cash.me/$JohnPaulW). My goal is to share part of any money I earn with my favorite open source projects. (More about that in the future.)