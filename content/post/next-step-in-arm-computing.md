+++
menu = ""
banner = "img/panther-mpc.png"
images = ["img/panther-mpc.png"
]
date = "2016-12-07T00:38:09-05:00"
title = "Next Step in ARM Computing"
tags = [ "Panther MPC", "ARM"
]
categories = [ "history of computers", "Kickstarter"
]
description = ""

+++

[**Please note:** The  Panther Alpha failed to reach it's original Kickstarter goal. They are planning to resubmit the Kickstart campaign in Q4 2018. - 1805 update]

In the history of computing, there has been a number of major milestones. These milestones helped to direct the future direction of computing for years to come. The Panther MPC is shaping up to be one of those milestones.

<!--more-->

Lately, I have been reading quite a bit about the history of the history of computing, specifically personal computing. Today we take PCs, laptops and even cell phones for granted. That wasn't always the case. 

## The First Step

The first truly personal computing device was the [Altair 8800](https://en.wikipedia.org/wiki/Altair_8800) (1974). It was more of a kit than a complete computer. When you purchased an Altair 8800, you basically got the case and a bunch of boards and cables, along with some instructions on how to put it together. In order to make it work, you had to assemble it yourself. Eventually, third parties started making add-ons to expand the capabilities of the Altair. The Altair laid the foundation of what would become the personal computer industry

What the Altair was for personal computing, the [Raspberry Pi](https://en.wikipedia.org/wiki/Raspberry_Pi) was for ARM computing. Released in 2012, the Raspberry Pi was the product of a UK organisation that wanted to promote  basic computer science in schools and in developing countries. It succeeded beyond its creators' dreams. Like the Altair, it needed to be assembled to work like a regular computer. Just like the Altair, the Raspberry Pi has quite a few add-ons available to extend capabilities.

The one thing that the Altair and Raspberry Pi have in common is that they have helped promote interest in their respective areas (personal computer for the Altair and ARM-powered computing for the Raspberry Pi). Unfortunately, that interest is limited to hobbyists and tinkerers. In order to interest more people, the technology and usability need to take a big step.

## The Next Step

Let's go back to early personal computing. The Altair gave people the chance to bring their own computer home, but what could they do with it? At first, the only way to input data into an Altair was via a series of switches on the front of the case. Eventually, paper tape and a keyboard were added. Even so, it was still hard to use. In order to attract the general public a big step in usability needed to be made. That came in 1981 when the [IBM PC](https://en.wikipedia.org/wiki/IBM_Personal_Computer) was introduced.

The IBM PC was a complete system. It did not need to be assembled. It only needed to be plugged in and powered up to give the user access to it's computing power. IBM sold 100,000 systems the first years. by the end of 1982, [IBM was selling one ever minute](http://www.nytimes.com/2011/08/01/arts/the-clunky-pc-that-started-it-all.html?pagewanted=all&_r=0). The IBM PC set the standard for all PCs the followed. So much so that for years, PCs were known as IBM-compatibles. 

## What is ARM?

Before we talk about what the next step is for ARM computing, let me explain why ARM is and why it is so important.

[ARM](https://en.wikipedia.org/wiki/ARM_architecture) stands for Advanced RISC Machine. RISC, in turn, stands for reduced instruction set computing. This design allows processors to have few transistors than the chip found in most modern computers. This reduction means that ARM chips can run cooler, take less power, and cost less. ARM chips are usually used on devices that are meant to be light, portable, and battery powered. If you have a modern smartphone, then you have a device with an ARM chip.

If these ARM chips are so great, why are they only in mobile devices and not in laptops and PCs? Mainly because Windows only supports the x86 technology currently found in laptops and PCs. If Windows doesn't support it, then hardware manufacturers don't make them because most people won't want them. :(

The Windows + Intel's x86 technology has been the standard for a long time, but that changed when the Raspberry Pi was introduced. People started seeing how useful ARM could be. It would do more than take calls. Linux programmers started porting their Linux distros and tools to ARM. 

Even Microsoft is getting into the act. A couple of years ago, they released a version of Windows to run on the Raspberry Pi 3. This was a cut-down version of Windows that was limited to the command line. However, news came out last week that Microsoft is working on a [version of Windows 10 to work on Qualcomm's ARM processors](http://www.zdnet.com/article/windows-10-on-qualcomm-is-microsofts-attempt-to-drag-the-pc-into-the-21st-century/). One tech writer referred to it as "Microsoft's attempt to drag the PC into the 21st century".

## ARM's IBM  PC

Now that I've explained why ARM is important, let me introduce you to the ARM equivalent of the IBM PC: the [Panther Alpha](https://www.panther-mpc.com/). Unlike the Raspberry Pi or the Altair, the Panther Alpha is a complete computer, no assembling required. All you need to do is plug in the screen, keyboard and mouse and you are ready to rumble.

Unlike the Raspberry Pi, the Panther Alpha comes with Linux pre-installed, so you don't have to waste time figuring out which distro is for you. (It is also possible to dual-boot Android.) It also comes with 32 GB of built-in storage, which it expandable via USB or miroSD. The ARM technology allows the Panther Alpha to only use 5% of the power standard systems would require.

Here are the system stats for those who are interested.

- ARM Cortex-A53 @ 4 x 2GHz
- 2GB DDR3 Memory
- 1000 Mbit Ethernet
- 2.4/5GHz 802.11n Wireless
- Bluetooth 4.0, IR
- HDMI 2.0, 3.5mm audio
- microSD Card Reader
- 2x USB ports

You may notice that this computer has 2GB of RAM. That might seem low, but it is double what the newest Raspberry Pi has. Also, since the Panther Alpha runs a customized Linux distro, the RAM usage is lower than Windows users are used to. 

![](/img/panther-alpha-compare.png)

Currently, the Panther Alpha is on Kickstarter (This campaign was canceled because they only had half the money they needed by the end. They plan to release a new and improved Kickstarter campaign very soon.). If you know someone is who is interested in computing or in Linux, this would make a great present. If you have an office, this system could be a money saver in terms of power usage. 

I've put my money where my mouth is and invested in the future of ARM computing. Now it's your turn. 

---

If you found this article interesting, please share it on social media. You can also send a tip via [Paypal.me](https://paypal.me/johnpaulw) or [Square Cash](https://cash.me/$JohnPaulW).