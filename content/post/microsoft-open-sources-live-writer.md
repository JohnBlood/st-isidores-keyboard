+++
banner = "img/wlw-olw.png"
categories = ["news"]
date = "2016-07-14T23:35:22-04:00"
description = "Microsoft releases the code to WIndows Live Writer as open source."
images = ["img/olw_screenshot.png"]
menu = ""
tags = ["microsoft", "open source"]
title = "Microsoft Releases Blogging Application as Open Source"

+++

Yes, you read that correctly. I used "Microsoft" and "open-source" in the same sentence and it's not a joke. Microsoft under new CEO Satya Nadella has embraced a pro-open-source (and even a pro-Linux) stance that would have been impossible under previous leadership. As part of this new initiative, they created the [.NET Foundation](http://www.dotnetfoundation.org/) to support open-source projects. One of these projects is a fork of the successful but discontinued blogging software Windows Live Writer.

<!--more-->

Windows Live Writer was first published on November 6, 2007, and was based on Onfolio Writer. It was soon included in the Windows Live Essentials bundle. WLW allowed the user to create and edit blog posts for many different blogging services from one interface. It also including a wide range of extensions, making it very customizable. WLW went on to become very popular. ZDNet even wondered if it was going to be a ["killer app"](http://www.zdnet.com/article/writer-is-microsofts-first-live-killer-app/) for Microsoft. However, even popularity couldn't save it and Microsoft pulled the plug on it in 2012.

For those who enjoyed using WLW (myself included), we either had to find an alternative or run the old program. Then, out of the blue, Microsoft released and open-source fork of the program, named [Open Live Writer](http://openlivewriter.org/), on December 9, 2015. As it exists now, OLW is pretty much the same as the 2012 version with just a few minor changes to reflect the new name and developers. However, there is a lot going on behind the scenes. Since the original project was halted in 2012, there is a large amount of code that no longer works mainly because the blog services have changed their APIs. There are also several components that no longer exist, like the extension gallery and the "Blog This" API. 

![](img/olw_screenshot.png )

Besides cleaning up and updating the code, the team of volunteers working on OLW have a lot of additions and improvements planned that include:

* Support for new blogging services, like
	* Ghost
	* Tumblr
	* SquareSpace
* Support for Markdown
* Support for static site generators, like
	* Jekyll
	* Hugo
* Creating a new plugin system
* Create ports for Linux and Mac
* Create a portable version

So, if this project sounds like something you'd be interested in check out their [GitHub page](https://github.com/OpenLiveWriter/OpenLiveWriter). If you'd like to test drive the current version of Open Live Writer (0.6 at the writing of this article), you can download it from [their website](http://openlivewriter.org/). 

Here's hoping that Microsoft will release more popular, discontinued programs as open-source in the coming year.

---

If you found this article interesting, please share it on social media. You can also send a tip via [Paypal.me](https://paypal.me/johnpaulw) or [Square Cash](https://cash.me/$JohnPaulW).