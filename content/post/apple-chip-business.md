+++
tags = [ "Apple", "ARM" ]
date = "2018-04-03T23:21:15-05:00" 
title = "Apple Plans to Get into the Chip Business"
categories = [ "opinion", "history-of-computers" ]
description = "Could Apple me switching to ARM chips in the future?"
menu = ""
banner = "img/macbook.jpg"
images = ["img/macbook.jpg"]
+++

Recently,[Bloomberg](https://www.bloomberg.com/news/articles/2018-04-02/apple-is-said-to-plan-move-from-intel-to-own-mac-chips-from-2020) broke the news that Apple is planning to drop Intel and to start manufacturing and using their own chips. According to this report, the end goal of this move is to unify Apple's mobile and desktop platforms. This is an interesting idea that just might work in this post-PC world.

<!--more-->

## Apple's Processor History

In terms of desktop computers, Apple is currently on its *fourth* processor platform. The earliest Apple computers (the Apple I and Apple II) were powered by the MOS Technology [6502](https://en.wikipedia.org/wiki/MOS_Technology_6502) chip. When the Macintosh came on the scene in 1984, it needed more computing power to drive the new graphical interface. this led to the adoption of the Motorola processors. 

By the early 90s, the Motorola chips were falling behind in performance. Apple needed something new. In the quest to find a new processor, Apple started work on two projects. The first was a partnership between IBM, Apple, and Motorola with the goal of making IBM's PowerPC processor architecture a new standard in the industry. As part of the deal, the group worked on the creation of a portable multimedia engine named Script/X. The group also worked on porting the development version of Apple's next-generation operating system (Pink) to PowerPC. 

(As part of the partnership, Pink was renamed Taligent. Apple later lost interest in Taligent and it became the sole property of IBM. Apple continued with a new attempt at an operating system to replace the aging System 7, codenamed Copeland. Copeland was canceled when Apple purchased Steve Jobs' NeXT and used NeXTstep as the foundation for Mac OS X. But I digress.)

At the same time as the IBM, Apple, and Motorola partnership, there was a small skunkworks project at Apple, codenamed Star Trek. The goal of Star Trek was to port Mac OS to the Intel architecture. After a good amount of progress was made, the project was abruptly canceled after it's leader and main promoter left Apple for Microsoft.

In 2005, Steve Jobs announced that future Apple computers would use Intel chips. According to [Jobs](https://www.macworld.com/article/1046961/intelvsamd.html), the main reason for the change was IBM's slow speed of chip development. Apple wanted to take advantage of Intel less power-hungry chips, especially for laptops. 

![](/img/iphone-macboook.jpg)

## The Current Move

Based on this quick history, it's obvious that Apple wants to be master of its own destiny. In the Bloomberg article, the reason given for the move away from Intel is the same as the previous moves: slow progress.

Over the last couple of years, it seems as though the rate of processor development has slowed considerably. It's wasn't that long ago that PC processors had ratings below 1GHz. However, In the decade or so since the introduction of the 1GHz processor, we have not seen that number do much higher. There seems to be an impassible 4GHz barrier. Maybe Apple thinks they have the know-how to accomplish this.

The question is: "If they leave Intel, what in the world are they going to do?" Are they going to switch to ARM? ARM powers smartphones and embedded systems, but isn't quite powerful enough for desktop use. Are they going to jump to an as yet unknown standard? The answer to the question is: "I don't know". The fact of the matter is that all we know is that Apple is planning to move away from Intel. What they are going to do remains a secret known only to a few people in Cupertino.

## Is This a Scare Tactic on Apple's Part?

Since there is no real information about this planned move, the question immediately comes to mind "Could Apple have made this announcement to intimidate Intel?" According to the Bloomberg article, Apple accounts for 5% of all Intel revenue. Maybe Apple thinks that they should be getting more of a discount on the chips they buy. Maybe Apple wants Intel to add certain features, but Intel refuses to. However, would a 5% drop in revenue be enough to alarm Intel?

When I first saw the Bloomberg article, I wondered if it was an April Fool's prank. I even checked the publication date. It came out on April 2, so if it was an April Fool's joke it was late.

Right now, we have so little to go on, there is no way to decide if this is a good move for Apple or not. The announcement did cause Intel stock to drop, so some blood has been spilled already.

![](/img/iphone.jpg)

## Final Thoughts

As I stated above, with so little information, it is hard to form an opinion on the topic. But then again, I like a challenge. 

For years, Apple was the odd man out as all other companies gave in to the unstoppable juggernaut that was the Windows/Intel platform. This was a conscious choice on Apple's part. They actively campaigned to show that their platform was superior to [Wintel](https://en.wikipedia.org/wiki/Wintel). The move to Intel years later was seen as a surrender to some, but it gave Apple access to a new generation of software writers and users. 

With the fall of desktop (and laptop) computing and the rise of mobile and cloud computing, Apple sees an opportunity to unify it's computing experiences across all devices. With the creation of a single codebase to run on a single, tightly integrated chip architecture; Apple sees an opportunity to recreate the Windows/Intel juggernaut in their image, to tame the beast to do their will.

It's an interesting thought experiment. The problem is that this project requires two things: time and money. They certainly have the money, but do they have the time? Both Microsoft and Google are working projects to capture the future of computing. Microsoft is working to get Windows running on ARM hardware. Google is currently working to combine Android and ChromeOS and has also started work on a new mobile operating system named Fuchsia. 

Closer to home, people are saying that Apple needs a new product line fast if they want to stay relevant. There are only so many improvements you can make to the iPhone and the Apple Watch hasn't found much traction.

Time will tell. Let's hope that Apple does something cool and unforgettable.

---

If you found this article interesting, please share it on social media. You can also send a tip via [Paypal.me](https://paypal.me/johnpaulw) or [Square Cash](https://cash.me/$JohnPaulW).